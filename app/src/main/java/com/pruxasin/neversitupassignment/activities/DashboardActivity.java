package com.pruxasin.neversitupassignment.activities;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pruxasin.neversitupassignment.R;
import com.pruxasin.neversitupassignment.adapter.CustomerAdapter;
import com.pruxasin.neversitupassignment.database.DatabaseManager;
import com.pruxasin.neversitupassignment.item.CustomerItem;
import com.pruxasin.neversitupassignment.utils.Keys;
import com.pruxasin.neversitupassignment.utils.NeversitupSharedPreferences;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DashboardActivity extends AppCompatActivity {

    @BindView(R.id.activity_dashboard_customer_recycler_view)
    RecyclerView mCustomerRecyclerView;

    private Context mContext;
    private List<CustomerItem> customerItemList = new ArrayList<>();
    private DatabaseManager mDatabaseManager;
    private CustomerAdapter mCustomerAdapter;
    private NeversitupSharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);

        setTitle("Dashboard");

        mContext = this;

        mSharedPreferences = NeversitupSharedPreferences.getInstance(mContext);

        mDatabaseManager = new DatabaseManager(mContext);
        mDatabaseManager.open();

        fetchDataFromDatabase();
        setCustomerRecyclerView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashboard_menu, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.dashboard_menu_logout:
                clearSharedPreferencesAndCloseActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void clearSharedPreferencesAndCloseActivity() {
        mSharedPreferences.setToken("");
        this.finish();
    }

    private void setCustomerRecyclerView() {
        mCustomerAdapter = new CustomerAdapter(mContext, customerItemList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        mCustomerRecyclerView.setLayoutManager(mLayoutManager);
        mCustomerRecyclerView.setAdapter(mCustomerAdapter);

        DividerItemDecoration itemDecoration = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);
        mCustomerRecyclerView.addItemDecoration(itemDecoration);
    }

    private void fetchDataFromDatabase() {
        customerItemList = mDatabaseManager.getCustomerItemList();

        Log.d(Keys.TAG, "data size = " + customerItemList.size());
    }
}
