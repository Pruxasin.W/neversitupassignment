package com.pruxasin.neversitupassignment.activities;

import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.pruxasin.neversitupassignment.R;
import com.pruxasin.neversitupassignment.apicall.CustomerDetailCall;
import com.pruxasin.neversitupassignment.interfaces.CustomerDetailInterface;
import com.pruxasin.neversitupassignment.pojo.CustomerDetailPojo;
import com.pruxasin.neversitupassignment.utils.Keys;
import com.pruxasin.neversitupassignment.utils.NeversitupSharedPreferences;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomerDetailActivity extends AppCompatActivity implements CustomerDetailInterface {

    @BindView(R.id.activity_customer_detail_text_view_description)
    TextView mDescriptionTextView;

    private Context mContext;
    private NeversitupSharedPreferences mSharedPreferences;
    private String mCustomerName;
    private String mCustomerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_detail);
        ButterKnife.bind(this);

        mContext = this;

        mCustomerName = getIntent().getStringExtra(Keys.CUSTOMER_NAME);
        mCustomerId = getIntent().getStringExtra(Keys.CUSTOMER_ID);

        setTitle(mCustomerName);

        mSharedPreferences = NeversitupSharedPreferences.getInstance(mContext);

        fetchCustomerDetail();
    }

    private void fetchCustomerDetail() {
        String token = mSharedPreferences.getToken();


        CustomerDetailCall call = new CustomerDetailCall(mContext, this);
        call.doGetCustomerDetail(token, mCustomerId);

    }

    @Override
    public void setDetailToView(CustomerDetailPojo.Data data) {

    }

    @Override
    public void setDetailToView(String description) {
        mDescriptionTextView.setText(description);
    }
}
