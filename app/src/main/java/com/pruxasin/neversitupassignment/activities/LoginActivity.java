package com.pruxasin.neversitupassignment.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.pruxasin.neversitupassignment.R;
import com.pruxasin.neversitupassignment.apicall.LoginCall;
import com.pruxasin.neversitupassignment.database.DatabaseManager;
import com.pruxasin.neversitupassignment.interfaces.LoginInterface;
import com.pruxasin.neversitupassignment.item.CustomerItem;
import com.pruxasin.neversitupassignment.utils.Keys;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements LoginInterface {

    @BindView(R.id.activity_login_button_login)
    Button mLoginButton;

    private Context mContext;
    private DatabaseManager mDatabaseManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mContext = this;

        mDatabaseManager = new DatabaseManager(mContext);
        mDatabaseManager.open();

        mLoginButton.setOnClickListener(onClickListenerLoginButton);
    }

    private View.OnClickListener onClickListenerLoginButton = view -> {
        doLogin();
    };

    private void doLogin() {
        String username = "test";
        String password = "123456";

        LoginCall loginCall = new LoginCall(mContext, this);
        loginCall.doCallLogin(username, password);
    }

    private void openDashboardActivity() {
        Intent intent = new Intent(mContext, DashboardActivity.class);
        startActivity(intent);
    }

    @Override
    public void saveDataToDatabase(List<CustomerItem> customerItemList) {

        checkIfHasData();

        for (CustomerItem item : customerItemList) {
            Log.d(Keys.TAG, "id, name: " + item.id + ", " + item.name);
            mDatabaseManager.insert(item.id, item.name);
        }

        Log.d(Keys.TAG, "save data to database success");

        openDashboardActivity();
    }

    private void checkIfHasData() {
        if (mDatabaseManager.getCustomerItemList().size() > 0) {
            mDatabaseManager.deleteAllRecords();
        }
    }
}
