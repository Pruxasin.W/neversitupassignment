package com.pruxasin.neversitupassignment.utils;

public class Keys {
    public static final String TAG = "_neversitup";
    public static final String TOKEN = "token";
    public static final String CUSTOMER_ID = "customer_id";
    public static final String CUSTOMER_NAME = "customer_name";
}
