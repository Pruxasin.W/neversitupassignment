package com.pruxasin.neversitupassignment.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class NeversitupSharedPreferences {

    private static NeversitupSharedPreferences mInstance;
    private static SharedPreferences mSharedPreferences;

    public NeversitupSharedPreferences(final Context context) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static NeversitupSharedPreferences getInstance(final Context context) {
        if (mInstance == null) {
            mInstance = new NeversitupSharedPreferences(context.getApplicationContext());
        }
        return mInstance;
    }

    public String getToken() {
        return mSharedPreferences.getString(Keys.TOKEN, "");
    }

    public void setToken(String token) {
        mSharedPreferences.edit().putString(Keys.TOKEN, token).apply();
    }
}
