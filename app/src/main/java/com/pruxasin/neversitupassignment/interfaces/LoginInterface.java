package com.pruxasin.neversitupassignment.interfaces;

import com.pruxasin.neversitupassignment.item.CustomerItem;

import java.util.List;

public interface LoginInterface {
    void saveDataToDatabase(List<CustomerItem> customerItemList);
}
