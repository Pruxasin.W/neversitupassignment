package com.pruxasin.neversitupassignment.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.pruxasin.neversitupassignment.item.CustomerItem;

import java.util.ArrayList;

public class DatabaseManager {

    private DatabaseHelper mDbHelper;

    private Context mContext;

    private SQLiteDatabase mDatabase;

    public DatabaseManager(Context context) {
        mContext = context;
    }

    public DatabaseManager open() throws SQLException {
        mDbHelper = new DatabaseHelper(mContext);
        mDatabase = mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        mDbHelper.close();
    }

    public void insert(String customer_id, String customer_name) {
        ContentValues contentValue = new ContentValues();
        contentValue.put(DatabaseHelper.CUSTOMER_ID, customer_id);
        contentValue.put(DatabaseHelper.CUSTOMER_NAME, customer_name);
        mDatabase.insert(DatabaseHelper.TABLE_NAME, null, contentValue);
    }

    public Cursor fetch() {
        String[] columns = new String[]{DatabaseHelper._ID, DatabaseHelper.CUSTOMER_ID, DatabaseHelper.CUSTOMER_NAME};
        Cursor cursor = mDatabase.query(DatabaseHelper.TABLE_NAME, columns, null, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public int update(long _id, String customer_id, String customer_name) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.CUSTOMER_ID, customer_id);
        contentValues.put(DatabaseHelper.CUSTOMER_NAME, customer_name);
        int i = mDatabase.update(DatabaseHelper.TABLE_NAME, contentValues, DatabaseHelper._ID + " = " + _id, null);
        return i;
    }

    public void deleteAllRecords() {
        mDatabase.execSQL("delete from " + DatabaseHelper.TABLE_NAME);
    }

    public ArrayList<CustomerItem> getCustomerItemList() {
        ArrayList<CustomerItem> customerItemArrayList = new ArrayList<>();

        String sql = "SELECT * FROM " + DatabaseHelper.TABLE_NAME;

        Cursor cursor = fetch();
        try {
            if (cursor.moveToFirst()) {
                do {
                    CustomerItem customerItem = new CustomerItem();
                    customerItem.setId(cursor.getString(cursor.getColumnIndex(DatabaseHelper.CUSTOMER_ID)));
                    customerItem.setName(cursor.getString(cursor.getColumnIndex(DatabaseHelper.CUSTOMER_NAME)));

                    customerItemArrayList.add(customerItem);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            cursor.close();
            e.printStackTrace();
        } finally {
            cursor.close();
        }

        return customerItemArrayList;
    }

}
