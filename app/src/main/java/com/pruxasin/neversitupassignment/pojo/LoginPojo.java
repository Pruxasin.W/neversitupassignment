package com.pruxasin.neversitupassignment.pojo;

import com.google.gson.annotations.SerializedName;
import com.pruxasin.neversitupassignment.item.CustomerItem;

import java.util.ArrayList;
import java.util.List;

public class LoginPojo {
    @SerializedName("customers")
    public List<CustomerItem> customers = new ArrayList<>();

    @SerializedName("status")
    public Integer status;

    @SerializedName("token")
    public String token;
}
