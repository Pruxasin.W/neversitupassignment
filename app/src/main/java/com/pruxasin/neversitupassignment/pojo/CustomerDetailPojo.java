package com.pruxasin.neversitupassignment.pojo;

import com.google.gson.annotations.SerializedName;

public class CustomerDetailPojo {
    @SerializedName("data")
    public Data data;

    @SerializedName("status")
    public Integer status;

    @SerializedName("description")
    public String description;

    public class Data {

        @SerializedName("customerGrade")
        public String customerGrade;

        @SerializedName("id")
        public String id;

        @SerializedName("isCustomerPremium")
        public Boolean isCustomerPremium;

        @SerializedName("name")
        public String name;

        @SerializedName("sex")
        public String sex;

    }
}
