package com.pruxasin.neversitupassignment.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pruxasin.neversitupassignment.R;
import com.pruxasin.neversitupassignment.activities.CustomerDetailActivity;
import com.pruxasin.neversitupassignment.item.CustomerItem;
import com.pruxasin.neversitupassignment.utils.Keys;

import java.util.List;

public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.ViewHolder> {
    private Context mContext;
    private List<CustomerItem> mCustomerItemList;

    public CustomerAdapter(Context context, List<CustomerItem> customerItemList) {
        this.mContext = context;
        this.mCustomerItemList = customerItemList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView mCustomerNameTextView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mCustomerNameTextView = itemView.findViewById(R.id.list_item_customer_name);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int index) {
        CustomerItem customerItem = mCustomerItemList.get(index);

        final String customerName = customerItem.name;

        viewHolder.mCustomerNameTextView.setText(customerName);
        viewHolder.mCustomerNameTextView.setOnClickListener(view -> {
            openDetailActivity(customerItem.id, customerName);
        });
    }

    private void openDetailActivity(String customerId, String customerName) {
        Intent intent = new Intent(mContext, CustomerDetailActivity.class);
        intent.putExtra(Keys.CUSTOMER_ID, customerId);
        intent.putExtra(Keys.CUSTOMER_NAME, customerName);
        mContext.startActivity(intent);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_customer, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return mCustomerItemList == null ? 0 : mCustomerItemList.size();
    }
}