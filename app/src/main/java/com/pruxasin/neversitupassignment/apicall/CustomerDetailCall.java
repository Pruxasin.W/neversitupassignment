package com.pruxasin.neversitupassignment.apicall;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.pruxasin.neversitupassignment.interfaces.CustomerDetailInterface;
import com.pruxasin.neversitupassignment.pojo.CustomerDetailPojo;
import com.pruxasin.neversitupassignment.utils.Keys;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CustomerDetailCall {

    private ApiInterfaces mApiInterface;
    private Context mContext;
    private CustomerDetailInterface mCustomerDetailInterface;

    public CustomerDetailCall(Context context, CustomerDetailInterface customerDetailInterface) {
        this.mContext = context;
        this.mCustomerDetailInterface = customerDetailInterface;
    }

    public void doGetCustomerDetail(final String token, final String customerId) {
        mApiInterface = ApiService.getApiService().create(ApiInterfaces.class);

        mApiInterface.doGetCustomerDetail(token, customerId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<CustomerDetailPojo>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(CustomerDetailPojo customerDetailPojo) {
                        if (customerDetailPojo != null) {
                            if (customerDetailPojo.status == 200) {
                                Toast.makeText(mContext, "get detail success", Toast.LENGTH_SHORT).show();

                                setDetailToView(customerDetailPojo.data);
                            } else {
                                Toast.makeText(mContext, "get detail failed, status = " + customerDetailPojo.status, Toast.LENGTH_SHORT).show();
                                Log.d(Keys.TAG, "get detail failed, status = " + customerDetailPojo.status);

                                setDetailToView(customerDetailPojo.description);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void setDetailToView(String description) {
        mCustomerDetailInterface.setDetailToView(description);
    }

    public void setDetailToView(CustomerDetailPojo.Data data) {
        mCustomerDetailInterface.setDetailToView(data);
    }
}
