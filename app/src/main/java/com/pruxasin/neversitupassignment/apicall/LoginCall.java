package com.pruxasin.neversitupassignment.apicall;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.pruxasin.neversitupassignment.interfaces.LoginInterface;
import com.pruxasin.neversitupassignment.item.CustomerItem;
import com.pruxasin.neversitupassignment.pojo.LoginPojo;
import com.pruxasin.neversitupassignment.utils.Keys;
import com.pruxasin.neversitupassignment.utils.NeversitupSharedPreferences;

import java.util.Arrays;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class LoginCall {

    private ApiInterfaces mApiInterface;
    private Context mContext;
    private LoginInterface mLoginInterface;

    public LoginCall(Context context, LoginInterface loginInterface) {
        this.mContext = context;
        this.mLoginInterface = loginInterface;
    }

    public void doCallLogin(final String username, final String password) {
        mApiInterface = ApiService.getApiService().create(ApiInterfaces.class);

        mApiInterface.doLogin(Arrays.asList("username", "password"))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<LoginPojo>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(LoginPojo loginPojo) {
                        if (loginPojo != null) {
                            if (loginPojo.status == 200) {
                                String token = loginPojo.token;
                                List<CustomerItem> customerItemList = loginPojo.customers;
                                setTokenToSharedPreference(token);

                                Toast.makeText(mContext, "Login Success", Toast.LENGTH_SHORT).show();

                                saveDataToDatabase(customerItemList);
                            } else {
                                Toast.makeText(mContext, "Login Failed, Status = " + loginPojo.status, Toast.LENGTH_SHORT).show();
                                Log.d(Keys.TAG, "login failed, status = " + loginPojo.status);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

/*        Call<LoginPojo> call = mApiInterface.doLogin(username, password);
        call.enqueue(new Callback<LoginPojo>() {
            @Override
            public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
                if (response.code() == 200) {
                    LoginPojo loginPojo = response.body();
                    if (loginPojo != null) {
                        if (loginPojo.status == 200) {
                            String token = loginPojo.token;
                            List<CustomerItem> customerItemList = loginPojo.customers;
                            setTokenToSharedPreference(token);

                            Toast.makeText(mContext, "Login Success", Toast.LENGTH_SHORT).show();

                            saveDataToDatabase(customerItemList);
                        } else {
                            Toast.makeText(mContext, "Login Failed, Status = " + loginPojo.status, Toast.LENGTH_SHORT).show();
                            Log.d(Keys.TAG, "login failed, status = " + loginPojo.status);
                        }
                    }
                } else {
                    Log.d(Keys.TAG, "login failed " + response.code());
                }
            }

            @Override
            public void onFailure(Call<LoginPojo> call, Throwable t) {
                Log.d(Keys.TAG, "login failed " + t.getMessage());
            }
        });*/

    }

    private void saveDataToDatabase(List<CustomerItem> customerItemList) {
        mLoginInterface.saveDataToDatabase(customerItemList);
    }

    private void setTokenToSharedPreference(final String token) {
        NeversitupSharedPreferences sharedPreferences = NeversitupSharedPreferences.getInstance(mContext);
        sharedPreferences.setToken(token);
    }
}
