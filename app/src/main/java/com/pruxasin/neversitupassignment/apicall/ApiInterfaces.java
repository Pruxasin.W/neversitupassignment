package com.pruxasin.neversitupassignment.apicall;

import com.pruxasin.neversitupassignment.pojo.CustomerDetailPojo;
import com.pruxasin.neversitupassignment.pojo.LoginPojo;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterfaces {

    @POST("login")
    Observable<LoginPojo> doLogin(@Body List<String> body);

/*    @POST("/login")
    @FormUrlEncoded
    Call<LoginPojo> doLogin(@Field("username") String username, @Field("password") String password);*/


    @POST("getCustomerDetail")
    @FormUrlEncoded
    Observable<CustomerDetailPojo> doGetCustomerDetail(@Field("token") String token, @Field("customerId") String customerId);

/*    @POST("getCustomerDetail")
    Observable<CustomerDetailPojo> doGetCustomerDetail(@Body List<String> body);*/
}
