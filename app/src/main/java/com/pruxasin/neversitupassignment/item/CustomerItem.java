package com.pruxasin.neversitupassignment.item;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class CustomerItem implements Parcelable {

    @SerializedName("id")
    public String id;

    @SerializedName("name")
    public String name;

    public CustomerItem() {
    }

    protected CustomerItem(Parcel in) {
        id = in.readString();
        name = in.readString();
    }

    public static final Creator<CustomerItem> CREATOR = new Creator<CustomerItem>() {
        @Override
        public CustomerItem createFromParcel(Parcel in) {
            return new CustomerItem(in);
        }

        @Override
        public CustomerItem[] newArray(int size) {
            return new CustomerItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Creator<CustomerItem> getCREATOR() {
        return CREATOR;
    }
}
